class_name AstExpressionGroup
extends AstExpression


var inner: AstExpression



func dbg() -> String:
	return "(" + inner.dbg() + ")"
