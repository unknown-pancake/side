class_name AstOperators



static func get_binary_precedence(op: Binary) -> int:
	match op:
		Binary.mul, Binary.div, Binary.mod:
			return 1
		Binary.add, Binary.sub:
			return 2
		Binary.shl, Binary.shr:
			return 3
		Binary.lt, Binary.le, Binary.gt, Binary.ge:
			return 4
		Binary.eq, Binary.ne:
			return 5
		Binary.bw_and:
			return 6
		Binary.bw_xor:
			return 7
		Binary.bw_or:
			return 8
		Binary.l_and:
			return 9
		Binary.l_or:
			return 10
		_:
			push_error("Invalid binary operator ", op)
			return -1

static func get_unary_precedence(_op: Unary) -> int:
	return 0



static func get_binary_representation(op: Binary) -> String:
	match op:
		Binary.mul: return "×"
		Binary.div: return "÷"
		Binary.mod: return "%"
		Binary.add: return "+"
		Binary.sub: return "-"
		Binary.shl: return "<<"
		Binary.shr: return ">>"
		Binary.lt: return "<"
		Binary.le: return "≤"
		Binary.gt: return ">"
		Binary.ge: return "≥"
		Binary.eq: return "="
		Binary.ne: return "≠"
		Binary.bw_and: return "&"
		Binary.bw_xor: return "^"
		Binary.bw_or: return "|"
		Binary.l_and: return "and"
		Binary.l_or: return "or"
		_:
			push_error("Invalid binary operator ", op)
			return ""

static func get_unary_representation(op: Unary) -> String:
	match op:
		Unary.id: return "+"
		Unary.neg: return "-"
		Unary.bw_not: return "~"
		Unary.l_not: return "not"
		_:
			push_error("Invalid unary operator ", op)
			return ""



static func is_binary(op: int) -> bool:
	return op >= 0 and op < Unary.id

static func is_unary(op: int) -> bool:
	return op >= Unary.id



enum Binary {
	add, sub,
	mul, div, mod,
	bw_and, bw_or, bw_xor,
	shl, shr,
	l_and, l_or,
	eq, ne,
	lt, le,
	gt, ge,
}

enum Unary {
	id = 100, neg, 
	bw_not, l_not
}
