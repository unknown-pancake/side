class_name AstExpressionUnary
extends AstExpression



var value: AstExpression
var operator: AstOperators.Unary



func is_invalid() -> bool:
	return value != null
