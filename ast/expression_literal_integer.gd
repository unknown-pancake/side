class_name AstExpressionLiteralInteger
extends AstExpression



var raw_value: String
var value: int:
	get: return raw_value.to_int()
	set(v):
		raw_value = str(v)



func is_invalid() -> bool:
	return raw_value.is_empty() or not raw_value.is_valid_int()


func dbg() -> String:
	return raw_value
