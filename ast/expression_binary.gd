class_name AstExpressionBinary
extends AstExpression


var left: AstExpression
var right: AstExpression
var operator: AstOperators.Binary



func is_invalid() -> bool:
	return left != null and right != null


func dbg() -> String:
	return "{" + left.dbg() + " " + AstOperators.get_binary_representation(operator) + " " + right.dbg() + "}"
