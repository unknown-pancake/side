extends Control


var root: AstNode


func _ready() -> void:
	var expr = AstExpressionBinary.new()
	expr.left = AstExpressionLiteralInteger.new()
	expr.left.value = 1
	expr.operator = AstOperators.Binary.add
	expr.right = AstExpressionBinary.new()
	expr.right.left = AstExpressionLiteralInteger.new()
	expr.right.left.value = 2
	expr.right.operator = AstOperators.Binary.add
	expr.right.right = AstExpressionBinary.new()
	expr.right.right.left = AstExpressionLiteralInteger.new()
	expr.right.right.left.value = 3
	expr.right.right.operator = AstOperators.Binary.add
	expr.right.right.right = AstExpressionLiteralInteger.new()
	expr.right.right.right.value = 4
	
	root = expr
	
	$text.add_child(TextBuilder.build_expression(expr))
	
	$cursor.grab_focus()
	$cursor.context.goto_first_tile.call_deferred($text)

func _input(_ev: InputEvent) -> void:
	var txt = $text.get_child(0).node.dbg() + "\n"
	txt += $cursor.get_active_state_path() + "\n"
	
	match $cursor.context.position:
		TextCursorContext.P_UNKNOWN:     txt += "P_UNKNOWN"
		TextCursorContext.P_BEGIN:       txt += "P_BEGIN"
		TextCursorContext.P_MIDDLE:      txt += "P_MIDDLE"
		TextCursorContext.P_END:         txt += "P_END"
		TextCursorContext.P_BEGIN_END:   txt += "P_BEGIN_END"
	
	$Label.text = txt



func on_tile_clicked(t: TextTile, idx: int) -> void:
	$cursor.context.update_context_direct(t, false)
	
	if idx != -1:
		$cursor.context.set_input_index(idx)
	else:
		$cursor.context.update_context()
	
	$cursor.enforce_visible()
	$cursor.grab_focus()
