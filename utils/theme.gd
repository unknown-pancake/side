class_name UtilsTheme



static func get_color(node: Control, name: StringName, type: StringName) -> Color:
	return get_item(node, name, type, &"has_color", &"get_color", Color.BLACK)

static func get_constant(node: Control, name: StringName, type: StringName) -> int:
	return get_item(node, name, type, &"has_constant", &"get_constant", 0)

static func get_font(node: Control, name: StringName, type: StringName) -> Font:
	return get_item(node, name, type, &"has_font", &"get_font", null)

static func get_font_size(node: Control, name: StringName, type: StringName) -> int:
	return get_item(node, name, type, &"has_font_size", &"get_font_size", 16)

static func get_stylebox(node: Control, name: StringName, type: StringName) -> StyleBox:
	return get_item(node, name, type, &"has_stylebox", &"get_stylebox", null)

static func get_icon(node: Control, name: StringName, type: StringName) -> Texture2D:
	return get_item(node, name, type, &"has_icon", &"get_icon", null)



static func get_item(
  node: Control,
  name: StringName,
  type: StringName,
  has_fn: StringName,
  get_fn: StringName,
  default
):
	var theme = get_theme(node)
	var current = type

	while not current.is_empty():
		if theme.call(has_fn, name, current):
			return theme.call(get_fn, name, current)

		current = theme.get_type_variation_base(current)

	return default

static func get_theme(node: Control) -> Theme:
	var current: Control = node

	while current != null:
		if current.theme != null:
			return current.theme

		current = current.get_parent() as Control

	if ThemeDB.get_project_theme() != null:
		return ThemeDB.get_project_theme()

	return ThemeDB.get_default_theme()
