class_name TextBuilder



static func build_expression(expr: AstExpression) -> Control:
	if expr is AstExpressionLiteralInteger:
		return build_expression_literal_integer(expr)
	
	if expr is AstExpressionBinary:
		return build_expression_binary(expr)
	
	if expr is AstExpressionGroup:
		return build_expression_group(expr)
	
	if expr is AstExpressionUnary:
		return build_expression_unary(expr)
	
	return null

static func build_expression_literal_integer(expr: AstExpressionLiteralInteger) -> Control:
	var lit = TextIntegerLiteral.new()
	lit.node = expr
	lit.setup_from_node()
	return lit

static func build_expression_binary(expr: AstExpressionBinary) -> Control:
	var grp = TextExpressionBinary.new()
	grp.node = expr
	grp.setup_from_node()
	
	grp.set_left_control(build_expression(expr.left))
	grp.set_right_control(build_expression(expr.right))
	
	return grp

static func build_expression_group(expr: AstExpressionGroup) -> Control:
	var grp = TextExpressionGroup.new()
	grp.node = expr
	
	grp.set_inner_control(build_expression(expr.inner))
	
	return grp

static func build_expression_unary(expr: AstExpressionUnary) -> Control:
	var grp = TextGroup.new()

	var tok = TextToken.new()
	tok.content = AstOperators.get_unary_representation(expr.operator)
	tok.theme_type_variation = &"Symbol"
	grp.add_child(tok)

	grp.add_child(build_expression(expr.value))
	
	return grp
