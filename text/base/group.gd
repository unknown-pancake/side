class_name TextGroup
extends Container



func is_tile() -> bool:
	return false

func is_group() -> bool:
	return true



func mark_for_deletion() -> void:
	for child in get_children():
		child.mark_for_deletion()

func unmark_for_deletion() -> void:
	for child in get_children():
		child.unmark_for_deletion()



func set_underlined(v: bool) -> void:
	for child in get_children():
		child.set_underlined(v)
	
func set_invalid_underlined(v: bool) -> void:
	for child in get_children():
		child.set_invalid_underlined(v)



func move_child_at(c: Node, i: int) -> void:
	if c == null:
		return
	
	if i < 0:
		i = 0
	elif i > get_child_count():
		i = get_child_count()
	
	move_child(c, i)



func _notification(what: int) -> void:
	match what:
		NOTIFICATION_SORT_CHILDREN:
			_sort_horizontally()



func _get_minimum_size() -> Vector2:
	var x := 0.0
	var max_height := 0.0

	for child in get_children():
		if not child.visible:
			continue
		
		var min_size = child.get_minimum_size()
		if min_size.y > max_height:
			max_height = min_size.y
		
		x += min_size.x
	
	return Vector2(x, max_height)



func _sort_horizontally() -> void:
	var max_height := 0.0

	for child in get_children():
		if not child.visible:
			continue
		
		var min_size = child.get_minimum_size()
		if min_size.y > max_height:
			max_height = min_size.y

	var x := 0.0
	for child in get_children():
		if not child.visible:
			continue
		
		var child_size = child.get_minimum_size()
		child_size.y = max_height
		
		child.position = Vector2(x, 0)
		child.size = child_size
		
		x += child_size.x
