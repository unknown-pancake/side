class_name TextInput
extends TextToken



func is_input() -> bool:
	return true



func is_char_accepted(_at: int, _ch: String) -> bool:
	return true

func is_content_valid() -> bool:
	return true



func get_index_position(idx: int) -> float:
	var carets = _text_srv.shaped_text_get_carets(_text_rid, idx)
	return get_left_margin() + carets.leading_rect.position.x



func _get_input_index_on_click(ev: InputEventMouseButton) -> int:
	if not  _content.is_empty():
		var x = ev.position.x - get_left_margin()
		return ceil(_text_srv.shaped_text_hit_test_position(_text_rid, x))
	
	else:
		return 0

#	if not  _content.is_empty():
#		var font = get_font()
#		var font_size = get_font_size()
#		var font_color = get_font_color()
#
#		var content_size = font.get_string_size(_content, 0, -1, font_size)
#		var avail_height = size.y - get_top_margin() - get_bottom_margin()
#		var y = avail_height / 2.0 - content_size.y / 2.0 + font.get_ascent(font_size)
#
#		font.draw_string(
#			get_canvas_item(), 
#			Vector2(get_left_margin(), get_top_margin() + y),
#			_content, 0, -1, font_size, font_color
#		)
