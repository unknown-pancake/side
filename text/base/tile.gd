class_name TextTile
extends Control


var invalid: bool:
	get: return _invalid
	set(v):
		_invalid = v
		queue_redraw()
	
var underlined: bool:
	get: return _underlined
	set(v):
		_underlined = v
		queue_redraw()

var invalid_underlined: bool:
	get: return _invalid_underlined
	set(v):
		_invalid_underlined = v
		queue_redraw()

var context: int = 0
var marked_for_deletion: bool = false

var _invalid := false
var _underlined := false
var _invalid_underlined := false

var _invalid_sb: StyleBox = null
var _regular_sb: StyleBox = null 
var _invalid_underlined_sb: StyleBox = null
var _underlined_sb: StyleBox = null
var _left_margin: int = 0 
var _right_margin: int = 0
var _top_margin: int = 0
var _bottom_margin: int = 0
var _font: Font = null
var _font_size: int = 16
var _font_color: Color = Color.BLACK



func is_tile() -> bool:
	return true

func is_group() -> bool:
	return false

func is_input() -> bool:
	return false



func mark_for_deletion() -> void:
	marked_for_deletion = true

func unmark_for_deletion() -> void:
	marked_for_deletion = false


func set_underlined(v: bool) -> void:
	underlined = v

func set_invalid_underlined(v: bool) -> void:
	invalid_underlined = v



func get_left_margin() -> int:
	return _left_margin

func get_right_margin() -> int:
	return _right_margin

func get_top_margin() -> int:
	return _top_margin

func get_bottom_margin() -> int:
	return _bottom_margin

func get_font() -> Font:
	return _font

func get_font_size() -> int:
	return _font_size

func get_font_color() -> Color:
	return _font_color



func _ready() -> void:
	_cache_theme_items()

func _init() -> void:
	mouse_filter = Control.MOUSE_FILTER_STOP

func _draw() -> void:
	var rect = Rect2(Vector2.ZERO, size)
	var citm = get_canvas_item()

	if _invalid and _invalid_sb != null:
		_invalid_sb.draw(citm, rect)
	elif not _invalid and _regular_sb != null:
		_regular_sb.draw(citm, rect)

func _draw_overlay() -> void:
	var rect = Rect2(Vector2.ZERO, size)
	var citm = get_canvas_item()
	
	if _invalid_underlined and _invalid_underlined_sb != null:
		_invalid_underlined_sb.draw(citm, rect)
	elif _underlined and _underlined_sb != null:
		_underlined_sb.draw(citm, rect)

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_THEME_CHANGED:
			_cache_theme_items()

func _gui_input(ev: InputEvent) -> void:
	if ev is InputEventMouseButton and ev.pressed:
		_on_clicked(ev)



func _cache_theme_items() -> void:
	_invalid_sb = UtilsTheme.get_stylebox(self, "invalid", theme_type_variation)
	_regular_sb = UtilsTheme.get_stylebox(self, "regular", theme_type_variation)
	_invalid_underlined_sb = UtilsTheme.get_stylebox(self, "invalid_underlined", theme_type_variation)
	_underlined_sb = UtilsTheme.get_stylebox(self, "underlined", theme_type_variation)
	_left_margin = UtilsTheme.get_constant(self, "left_margin", theme_type_variation)
	_right_margin = UtilsTheme.get_constant(self, "right_margin", theme_type_variation)
	_top_margin = UtilsTheme.get_constant(self, "top_margin", theme_type_variation)
	_bottom_margin = UtilsTheme.get_constant(self, "bottom_margin", theme_type_variation)
	_font = UtilsTheme.get_font(self, "font", theme_type_variation)
	_font_size = UtilsTheme.get_font_size(self, "font_size", theme_type_variation)
	_font_color = UtilsTheme.get_color(self, "font_color", theme_type_variation)



func _on_clicked(ev: InputEventMouseButton) -> void:
	var parent = get_parent()
	while parent != null:
		if parent.has_method("on_tile_clicked"):
			parent.on_tile_clicked(self, _get_input_index_on_click(ev))
			break
		
		parent = parent.get_parent()

func _get_input_index_on_click(_ev: InputEventMouseButton) -> int:
	return -1
