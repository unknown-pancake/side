class_name TextToken
extends TextTile

signal content_modified(new_content: String)



var content: String:
	get: return _content
	set(v):
		_content = v
		content_modified.emit(v)
		
		if _text_srv != null:
			_shape()
			_update_min_size()
		queue_redraw()



var _content: String
var _min_size: Vector2

var _text_srv: TextServer
var _text_rid: RID



func _ready() -> void:
	_text_srv = TextServerManager.get_primary_interface()
	_text_rid = _text_srv.create_shaped_text()
	
	_shape()
	_update_min_size()

func _draw() -> void:
	super()

	if not  _content.is_empty():
		var font = get_font()
		var font_size = get_font_size()
		var font_color = get_font_color()

		var content_size = _text_srv.shaped_text_get_size(_text_rid)
		var avail_height = size.y - get_top_margin() - get_bottom_margin()
		var y = avail_height / 2.0 - content_size.y / 2.0 + font.get_ascent(font_size)

		font.draw_string(
			get_canvas_item(), 
			Vector2(get_left_margin(), get_top_margin() + y),
			_content, 0, -1, font_size, font_color
		)
	
	_draw_overlay()



func _get_minimum_size() -> Vector2:
	return _min_size



func _update_min_size() -> void:
	if not is_inside_tree():
		return
	
	var content_size = _text_srv.shaped_text_get_size(_text_rid)
	content_size.x += get_left_margin() + get_right_margin()
	content_size.y += get_top_margin() + get_bottom_margin()

	_min_size = content_size
	minimum_size_changed.emit()



func _shape() -> void:
	var font = get_font()
	if font == null:
		return
	
	_text_srv.shaped_text_clear(_text_rid)
	
	if _content.is_empty():
		_text_srv.shaped_text_add_string(
			_text_rid, " ", font.get_rids(), get_font_size(), 
			font.get_opentype_features(), "en"
		)
	else:
		_text_srv.shaped_text_add_string(
			_text_rid, _content, font.get_rids(), get_font_size(), 
			font.get_opentype_features(), "en"
		)
