class_name TextExpressionUtils



static func is_group_parenthesis(tok: TextToken) -> bool:
	return tok.is_in_group(&"group_parenthesis")

static func is_binary_operator(tok: TextToken) -> bool:
	return tok.is_in_group(&"binary_operator")

static func is_atom(node: Control) -> bool:
	return node.is_in_group(&"atom")

static func is_within_group(node: Control) -> bool:
	var parent = node.get_parent()
	return parent is TextGroup and parent.node is AstExpressionGroup



static func group_expression(node: Control) -> void:
	var parent = node.get_parent()
	
	var group = TextExpressionGroup.new()
	group.node = AstExpressionGroup.new()
	group.node.inner = node.node
	
	remove_child(parent, node)
	add_child(group, node)
	add_child(parent, group)

static func ungroup_expression(group: TextExpressionGroup) -> void:
	var inner = group.get_inner_control()
	var parent = group.get_parent()
	
	remove_child(group, inner)
	remove_child(parent, group)
	add_child(parent, inner)
	
	group.free()



static func insert_text_in_input(input: TextInput, txt: String, at: int) -> void:
	var before = input.content.substr(0, at)
	var after = input.content.substr(at)
	input.content = before + txt + after

static func remove_text_from_input(input: TextInput, at: int, length: int = 1) -> void:
	if at < 0:
		return
	
	var before = input.content.substr(0, at)
	var after = input.content.substr(at + length)
	input.content = before + after



static func mutate_binary_operator(bin: TextExpressionBinary, new_op: AstOperators.Binary, reorder := true) -> void:
	bin.node.operator = new_op
	bin.get_operator_control().content = AstOperators.get_binary_representation(new_op)
	
	if reorder: 
		reorder_expression_with_precedence(bin)

static func mutate_hole_to_integer_literal(
	hole: TextExpressionHole,
	value: int
) -> TextIntegerLiteral:
	var parent = hole.get_parent()
	
	remove_child(parent, hole)
	
	var int_lit = TextIntegerLiteral.new()
	int_lit.node = AstExpressionLiteralInteger.new()
	int_lit.node.value = value
	int_lit.setup_from_node()
	
	add_child(parent, int_lit)
	hole.free()
	
	return int_lit


static func reorder_expression_with_precedence_in_and_out_of_group(
	group: TextExpressionGroup
) -> void:
	reorder_expression_with_precedence(group.get_inner_control())
	
	var parent = group.get_parent()
	if parent is TextExpressionBinary:
		reorder_expression_with_precedence(parent)

static func reorder_expression_with_precedence(node: Control) -> void:
	if node is TextExpressionBinary:
		# TODO: check wether or not reordering children should be done or not
		var node_prec = AstOperators.get_binary_precedence(node.node.operator)
		
		# reorder the left expression
		while true:
			var left = node.get_left_control()
			if left.is_group():
				if left is TextExpressionBinary:
					# the left expression is considered part of the span
					var left_prec = AstOperators.get_binary_precedence(left.node.operator)
					
					if node_prec < left_prec:
						exchange_expression_binary_order(node, left)
						continue
			break
		
		# reorder the right expression
		while true:
			var right = node.get_right_control()
			if right.is_group():
				if right is TextExpressionBinary:
					# the right expression is considered part of the span
					var right_prec = AstOperators.get_binary_precedence(right.node.operator)
					
					if node_prec < right_prec:
						exchange_expression_binary_order(node, right)
						continue
			break
		
		var p = node.get_parent()
		if p is TextGroup and p.node is AstExpression:
			reorder_expression_with_precedence(p)

static func exchange_expression_binary_order(
	parent: TextExpressionBinary, 
	branch: TextExpressionBinary
) -> void:
	var is_left = parent.get_left_control() == branch
	
	var pparent = parent.get_parent()
	var parent_idx = parent.get_index()
	var extracted = branch.get_control(not is_left)
	
	remove_child(branch, extracted)
	remove_child(parent, branch)
	remove_child(pparent, parent)
	
	add_child(parent, extracted)
	add_child(branch, parent)
	add_child(pparent, branch, parent_idx)



static func move_node(node: TextTile, to_left: bool) -> void:
	if is_group_parenthesis(node):
		move_group_parenthesis(node, to_left)
	elif is_binary_operator(node):
		move_leaf_node(node.get_parent(), to_left)
	else:
		move_leaf_node(node, to_left)

static func move_group_parenthesis(node: TextTile, to_left: bool) -> void:
	var group: TextExpressionGroup = node.get_parent()
	
	# if the moved parenthesis is the left one
	if node.get_index() == 0:
		if to_left:
			if expand_group(group, true):
				reorder_expression_with_precedence_in_and_out_of_group(group)
		else:
			if reduce_group(group, true):
				reorder_expression_with_precedence_in_and_out_of_group(group)
			
	# if the moved parenthesis is the right one
	else:
		if to_left:
			if reduce_group(group, false):
				reorder_expression_with_precedence_in_and_out_of_group(group)
		else:
			if expand_group(group, false):
				reorder_expression_with_precedence_in_and_out_of_group(group)

static func move_leaf_node(node: Control, to_left: bool) -> void:
	if is_within_group(node):
		move_leaf_node(node.get_parent(), to_left) 
		return
	
	var target = find_atom_or_group(node, to_left, true)
	
	if target == null:
		printerr("Unable to find a suitable target for moving the node.")
		return
	
	exchange_nodes(node, target)



static func expand_group(
	group: TextExpressionGroup,
	to_left: bool
) -> bool:
	var inner = group.get_inner_control()
	var target = find_atom_or_group(group, to_left)
	
	if target == null:
		printerr("No suitable target found.")
		return false
	
	var tparent = target.get_parent()
	var ancestor = TextTreeUtils.find_common_ancestor(group, target)
	var aparent = ancestor.get_parent()
	
	if not ancestor is TextExpressionBinary:
		return false
	
	var ancestor_left = ancestor.get_control(to_left)
	var ancestor_right = ancestor.get_control(not to_left)
	
	remove_child(aparent, ancestor)
	remove_child(ancestor, ancestor_left)
	remove_child(ancestor, ancestor_right)
	remove_child(tparent, target)
	remove_child(group, inner)
	
	add_child(group, ancestor)
	if ancestor != tparent:
		add_child(tparent, ancestor_right)
		add_child(aparent, ancestor_left)
	else:
		add_child(aparent, ancestor_right)
	
	add_child(ancestor, target if to_left else inner)
	add_child(ancestor, inner if to_left else target)
	
	return true

static func reduce_group(
	group: TextExpressionGroup,
	from_left: bool
) -> bool:
	var inner = group.get_inner_control()
	var parent = group.get_parent()
	var target = find_atom_or_group(inner, not from_left, false, true)
	
	if target == null or target == inner:
		printerr("Cannot reduce this group.")
		return false
	
	var tparent = target.get_parent()
	var tparent2 = tparent.get_parent()
	var tsibbling = tparent.get_control(target.get_index() != 0)
	
	remove_child(parent, group)
	remove_child(tparent2, tparent)
	remove_child(tparent, tsibbling)
	
	add_child(parent, tparent)
	add_child(tparent, group)
	
	add_child(tparent2, tsibbling)
	
	return true



static func delete_node(
	node: Control,
) -> void:
	var parent = node.get_parent()
	var pparent = parent.get_parent()
	
	if parent is TextExpressionBinary:
		var is_left = parent.get_left_control() == node
		var other = parent.get_control(not is_left)
		
		remove_child(parent, other)
		remove_child(pparent, parent)
		add_child(pparent, other)
		parent.free()
	
	elif parent is TextExpressionGroup:
		delete_node(parent)

static func mark_node_for_deletion(
	node: Control
) -> void:
	var parent = node.get_parent()
	
	if parent is TextExpressionBinary:
		var is_left = parent.get_left_control() == node
		var other = parent.get_control(not is_left)
		
		parent.mark_for_deletion()
		other.unmark_for_deletion()
	
	elif parent is TextExpressionGroup:
		mark_node_for_deletion(parent)

static func unmark_node_for_deletion(
	node: Control
) -> void:
	var parent = node.get_parent()
	
	if parent is TextExpressionBinary:
		parent.unmark_for_deletion()
		
	elif parent is TextExpressionGroup:
		unmark_node_for_deletion(parent)



static func insert_binary_expression(
	current: Control,
	op: AstOperators.Binary,
	left: bool
) -> void:
	var parent = current.get_parent()
	remove_child(parent, current)
	
	var ast = AstExpressionBinary.new()
	ast.operator = op
	
	var hole = TextExpressionHole.new()
	hole.node = AstExpressionHole.new()
	
	var bin = TextExpressionBinary.new()
	bin.node = ast
	bin.setup_from_node()
	
	if left:
		add_child(bin, hole)
		add_child(bin, current)
	else:
		add_child(bin, current)
		add_child(bin, hole)
	
	add_child(parent, bin)
	reorder_expression_with_precedence(parent)


static func find_tile_in_group(
	from: Control, 
	to_left: bool,
	group: StringName, 
	search_in_parents := false,
	include_from := false
) -> TextInput:
	var pred = func(x):
		return x.is_tile() and x.is_in_group(group)
	
	if search_in_parents:
		return find_node_down_and_up(from, to_left, pred, include_from)
	else:
		return TextTreeUtils.find_node_down(from, to_left, pred, include_from)

static func find_atom_or_group(
	from: Control,
	to_left: bool,
	search_in_parents := false,
	include_from := false
) -> Control:
	var pred = func(x):
		if x.is_tile():
			return is_atom(x)
		else:
			return x is TextExpressionGroup
	
	if search_in_parents:
		return find_node_down_and_up(from, to_left, pred, include_from)
	else:
		return TextTreeUtils.find_node_down(from, to_left, pred, include_from)

static func find_node_down_and_up(
	from: Control,
	to_left: bool,
	pred: Callable,
	include_from := false
) -> Control:
	var root_pred = func(x: Control):
		if x is TextGroup:
			return not x.node is AstExpression
		
		return true
		
	return TextTreeUtils.find_node_down_then_up(from, to_left, pred, root_pred, include_from)



static func add_child(parent: Control, child: Control, index_hint: int = -1) -> void:
	if child.get_parent() != null:
		if child.get_parent() == parent:
			return
		else:
			push_error("Trying to add an already owned child")
			return
	
	if parent is TextExpressionBinary:
		var no_left = parent.get_left_control() == null
		var no_right = parent.get_right_control() == null
		var hint_left = index_hint == 0
		var hint_right = index_hint == 1
		
		if (no_left and not no_right) or (no_left and hint_left) or (no_left and not hint_right):
			parent.set_left_control(child)
			parent.node.left = child.node
		elif (not no_left and no_right) or (no_right and not hint_left and hint_right):
			parent.set_right_control(child)
			parent.node.right = child.node
		else:
			push_error("Parent is full.")
	
	elif parent is TextExpressionGroup:
		if parent.get_inner_control() == null:
			parent.set_inner_control(child)
			parent.node.inner = child.node
		else:
			push_error("Parent is full.")
	
	else:
		#push_error("Unsupported parent node '", parent.get_class(), "'.")
		parent.add_child(child)
		parent.move_child(child, index_hint)

static func remove_child(parent: Control, child: Control) -> void:
	if child.get_parent() == null:
		return
	
	if parent is TextExpressionBinary:
		if parent.get_left_control() == child:
			parent.remove_child(child)
			parent.set_left_control(null)
			parent.node.left = null
		elif parent.get_right_control() == child:
			parent.remove_child(child)
			parent.set_right_control(null)
			parent.node.right = null
		else:
			push_error("The node is not a child of the parent.")
	
	elif parent is TextExpressionGroup:
		if parent.get_inner_control() == child:
			parent.remove_child(child)
			parent.set_inner_control(null)
			parent.node.inner = null
		else:
			push_error("The node is not a child of the parent.")
	
	else:
		#push_error("Unsupported parent node '", parent.get_class(), "'.")
		parent.remove_child(child)

static func replace_node(node: Control,	with: Control) -> void:
	var parent = node.get_parent()
	var index = node.get_index()
	
	remove_child(parent, node)
	add_child(parent, with)

	# shouldn't happen if the parent is a TileGroup
	if with.get_index() != index:
		parent.move_child(with, index)

static func exchange_nodes(node_a: Control, node_b: Control) -> void:
	var parent_a = node_a.get_parent()
	var parent_b = node_b.get_parent()
	
	if parent_a == parent_b:
		var idx_a = node_a.get_index()
		var idx_b = node_b.get_index()
		
		remove_child(parent_a, node_a)
		remove_child(parent_a, node_b)
		
		# add the child in the reverse order they were in initially as 
		# `add_child` adds in slot order
		if idx_a < idx_b:
			add_child(parent_a, node_b)
			add_child(parent_a, node_a)
		else:
			add_child(parent_a, node_a)
			add_child(parent_a, node_b)
	
	else:
		remove_child(parent_a, node_a)
		remove_child(parent_b, node_b)
		add_child(parent_a, node_b)
		add_child(parent_b, node_a)
