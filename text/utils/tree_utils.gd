class_name TextTreeUtils



static func find_node_down(
	from: Control,
	to_left: bool,
	pred: Callable,
	include_from := false
) -> Control:
	var parent = from.get_parent()
	var index = from.get_index()
	var delta = -1 if to_left else +1
	
	if not include_from:
		index += delta
	
	while index >= 0 and index < parent.get_child_count():
		var child = parent.get_child(index)
		
		if pred.call(child):
			return child
		
		if child is TextGroup and child.is_group() and child.get_child_count() > 0:
			if to_left:
				return find_node_down(child.get_child(-1), true, pred, true)
			else:
				return find_node_down(child.get_child(0), false, pred, true)
		
		index += delta
	
	return null

static func find_node_up(
	from: Control,
	pred: Callable,
	include_from := false
) -> Control:
	var current = from
	
	if not include_from:
		current = current.get_parent()
	
	if current == null:
		return null
	
	while true:
		if pred.call(current):
			return current
		
		current = current.get_parent()
		
		if current == null or not current is TextGroup:
			break
	
	return null

static func find_node_up_then_down(
	from: Control,
	to_left: bool,
	up_pred: Callable,
	down_pred: Callable,
	include_from := false,
	include_parent := false
) -> Control:
	var parent = find_node_up(from, up_pred, include_from)
	
	if parent == null:
		return null
	
	return find_node_down(parent, to_left, down_pred, include_parent)

static func find_node_down_then_up(
	from: Control,
	to_left: bool,
	pred: Callable,
	root_pred: Callable,
	include_from := false,
) -> Control:
	if include_from and pred.call(from):
		return from
	
	var node = from
	var last = null
	
	while true:
		if node != from and root_pred.call(node):
			break
		
		if node.get_child_count() > 0:
			var res = null
			
			if last == null:
				var first = node.get_child(-1 if to_left else 0)
				res = find_node_down(first, to_left, pred, true)
			else:
				res = find_node_down(last, to_left, pred, false)
			
			if res != null:
				return res
		
		last = node
		node = node.get_parent()
	
	return null



static func find_common_ancestor(
	node_a: Control,
	node_b: Control
) -> Control:
	var pred = func(x: Control):
		return x.is_ancestor_of(node_b)
	
	return find_node_up(node_a, pred)

static func find_tile(
	from: Control,
	to_left: bool,
	include_from := false
) -> TextTile:
	var root_pred = func(x: Control):
		return not x is TextGroup
	
	var pred = func(x: Control):
		return x is TextTile
	
	return find_node_down_then_up(from, to_left, pred, root_pred, include_from)
