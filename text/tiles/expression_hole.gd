class_name TextExpressionHole
extends TextInput


var node: AstExpressionHole



func is_char_accepted(_at: int, _ch: String) -> bool:
	return false

func is_content_valid() -> bool:
	# node always return invalid
	return false



func setup_from_node() -> void:
	content = node.content
	invalid = true



func _init() -> void:
	theme_type_variation = &"ExpressionHole"
	add_to_group(&"atom")
	invalid = true
	context = TextCursorContext.EXPR_HOLE

func _ready() -> void:
	super()
	content_modified.connect(_on_content_modified)



func _on_content_modified(new_content: String) -> void:
	node.content = new_content
