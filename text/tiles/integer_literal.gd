class_name TextIntegerLiteral
extends TextInput


var node: AstExpressionLiteralInteger


func is_char_accepted(_at: int, ch: String) -> bool:
	return ch.is_valid_int()

func is_content_valid() -> bool:
	return not node.is_invalid()



func setup_from_node() -> void:
	content = node.raw_value
	invalid = node.is_invalid()



func _init() -> void:
	theme_type_variation = &"Integer"
	context = TextCursorContext.EXPR_INT
	
	add_to_group(&"atom")

func _ready() -> void:
	super()
	content_modified.connect(_on_content_modified)



func _on_content_modified(new_content: String) -> void:
	node.raw_value = new_content
	invalid = node.is_invalid()
