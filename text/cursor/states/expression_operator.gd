extends TextCursorState



func get_state_name() -> String:
	return "operator"



func _unhandled_text_input(txt: String) -> bool:
	if _mutate_operator(txt):
		return true
	
	elif _repeat_into_hole():
		return true
	
	return false



func _mutate_operator(txt: String) -> bool:
	var bin = _context.current.get_parent()
	var op = _context.push_operator_char(txt)
	
	if op == -2:
		return false
	elif op != -1:
		TextExpressionUtils.mutate_binary_operator(bin, op)
		_cursor.enforce_visible()
		_cursor.queue_redraw_after(_context.current.resized)
	
	return true

func _repeat_into_hole() -> bool:
	if _context.next_is(TextCursorContext.EXPR_HOLE):
		_cursor.repeat_input()
		_context.goto_right_tile()
		return true
	
	return false
