extends TextCursorState



func get_state_name() -> String:
	return "hole"



func _unhandled_text_input(txt: String) -> bool:
	if txt.is_valid_int():
		var lit = TextExpressionUtils.mutate_hole_to_integer_literal(
			_context.current, txt.to_int()
		)
		
		# call them deferred to fix a bug : 
		# if the cursor is over an operator with a hole right after it, when
		# the user types a digit, then without deferred calls the cursor is
		# incorrectly positioned and drawn
		_cursor.queue_reposition_visible.call_deferred()
		_cursor.queue_redraw.call_deferred()
		_context.update_context_current(lit, false)
		_context.set_input_index_to_end()
		return true
	
	return false



func _handle_context_change(_from_activation: bool) -> void:
	_cursor.set_draw_info(_context.current, TextCursor.DM_BEAM)
