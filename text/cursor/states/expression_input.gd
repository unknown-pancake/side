extends TextCursorState


const StateHole = preload("./expression_input_hole.gd")

enum {
	S_HOLE = 0,
}



func get_state_name() -> String:
	return "input"



func notify_activation(cur: TextCursor, ctx: TextCursorContext) -> void:
	super(cur, ctx)
	
	match ctx.last_motion:
		TextCursorContext.M_LEFT:
			ctx.set_input_index_to_end()
		TextCursorContext.M_RIGHT:
			ctx.set_input_index_to_begin()

func notify_deactivation() -> void:
	super()



func _unhandled_key_event(ev: InputEventKey) -> bool:
	if ev.pressed:
		match ev.keycode:
			KEY_LEFT: if not ev.shift_pressed and _context.move_left_within_input():
				_cursor.queue_reposition_visible()
				return true
			
			KEY_RIGHT: if not ev.shift_pressed and _context.move_right_within_input():
				_cursor.queue_reposition_visible()
				return true
				
			KEY_BACKSPACE:
				return _delete_in_input(true)
			
			KEY_DELETE:
				return _delete_in_input(false)
	
	return false

func _pre_handle_text_input(txt: String) -> bool:
	var input := _context.current as TextInput
	var input_idx = _context.input_index
	
	if input.is_char_accepted(input_idx, txt):
		TextExpressionUtils.insert_text_in_input(input, txt, input_idx)
		_context.move_right_within_input()
		_cursor.queue_reposition_visible()
		return true
	
	return false

func _unhandled_cursor_reposition() -> bool:
	var input := _context.current as TextInput
	_cursor.position_over_input(input, _context.input_index)
	return true



func _sync_to_context() -> void:
	if _context.current_is(TextCursorContext.EXPR_HOLE):
		_activate_substate(S_HOLE)
	else:
		_deactivate_substate()

func _handle_context_change(_from_activation: bool) -> void:
	_cursor.set_draw_info(_context.current, TextCursor.DM_BEAM)



func _delete_in_input(left: bool) -> bool:
	var input := _context.current as TextInput
	var input_idx = _context.input_index
	
	var delete_idx = input_idx - 1 if left else input_idx
	
	if not input.content.is_empty():
		TextExpressionUtils.remove_text_from_input(input, delete_idx, 1)
		if left:
			_context.move_left_within_input()
		_cursor.queue_reposition_visible()
		return true
	
	else:
		TextExpressionUtils.mark_node_for_deletion(input)
		
		if not _context.goto_tile_after_deletion_of(input, left, false):
			TextExpressionUtils.unmark_node_for_deletion(input)
			return false
		
		TextExpressionUtils.delete_node(input)
		_cursor.queue_reposition_visible()
		_context.update_context_surrounding()
		
		# in the case the new current tile is still an input (thus no state 
		# change), make sure the state is reactivated
		if _cursor != null:
			_cursor.queue_state_resync()
	
	return false



func _init() -> void:
	add_child(StateHole.new())
