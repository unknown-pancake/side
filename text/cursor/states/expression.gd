extends TextCursorState



const StateInput = preload("./expression_input.gd")
const StateOperator = preload("./expression_operator.gd")

enum {
	S_INPUT = 0,
	S_BINARY_OP,
}



func get_state_name() -> String:
	return "expression"



func _unhandled_text_input(txt: String) -> bool:
	if _context.position_is(TextCursorContext.P_BEGIN) and _edge_insert(txt, true):
		return true
	
	elif _context.position_is(TextCursorContext.P_END) and _edge_insert(txt, false): 
		return true
	
	return false



func _sync_to_context() -> void:
	if _context.current_is_input():
		_activate_substate(S_INPUT)
	elif _context.current_is_operator():
		_activate_substate(S_BINARY_OP)
	else:
		_deactivate_substate() 



func _init() -> void:
	add_child(StateInput.new())
	add_child(StateOperator.new())



func _edge_insert(txt: String, before: bool) -> bool:
	match txt:
		"(": 
			if before: 
				_group_expression()
				return true
		")":
			if _context.next_is(TextCursorContext.EXPR_RIGHT_PAR):
				_cursor.queue_reposition_visible()
				_context.goto_right_tile(false)
				_context.goto_right_tile()
				return true
		_: 
			if _edge_insert_operator(txt, before):
				return true
	
	return false

func _edge_insert_operator(txt: String, before: bool) -> bool:
	var op = _context.push_operator_char(txt)
	
	if op == -2:
		return false
	elif op == -1:
		return true
	else:
		if AstOperators.is_binary(op):
			_insert_binary_expression(op, before)
		else:
			printerr("Unary insertion NYI")
		
		return true



func _group_expression() -> void:
	if _context.current_is_parenthesis():
		TextExpressionUtils.group_expression(_context.current.get_parent())
	else:
		TextExpressionUtils.group_expression(_context.current)

	_cursor.queue_reposition_visible.call_deferred()
	_context.update_context_surrounding()

func _insert_binary_expression(op: AstOperators.Binary, to_left: bool) -> void:
	if _context.current_is_parenthesis():
		TextExpressionUtils.insert_binary_expression(_context.current.get_parent(), op, to_left)
	else:
		TextExpressionUtils.insert_binary_expression(_context.current, op, to_left)
	
	_cursor.queue_reposition_visible()
	_cursor.queue_redraw()
	
	_context.update_context_surrounding(false)
	
	if to_left:
		_context.goto_left_tile()
	else:
		_context.goto_right_tile()
