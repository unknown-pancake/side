class_name TextCursor
extends Control

# TODO: implement the TextCursorContext class
# TODO: implement the TextDocument node
# TODO: move underline logic to TextDocument
# TODO: implement contextual info display in TextDocument using TextCursorContext
# TODO: make TextDocument draw the regular background
# TODO: make TextTile draw background if not regular

# editor
#   document
#      text nodes...
#   cursor
#      states... (on per context type)

const StateExpression = preload("./states/expression.gd")

enum {
	S_EXPRESSION = 0,
}



enum {
	DM_BEAM,
	DM_RECT
}



var context: TextCursorContext

var _blink_timer: Timer
var _pending_reposition: bool = false
var _pending_state_resync: bool = false
var _active_state: int = -1
var _repeat_input: bool = false

# theme cache
var _focused_color: Color = Color.BLACK
var _unfocused_color: Color = Color.BLACK
var _blink_frequency: int = 0
var _beam_width: int = 0
var _border_width: int = 0
var _buffer_font: Font = null
var _buffer_font_size: int = 16
var _buffer_color: Color = Color.BLACK
var _buffer_background: StyleBox = null
var _buffer_left_margin: int = 0
var _buffer_right_margin: int = 0
var _buffer_top_margin: int = 0
var _buffer_bottom_margin: int = 0
var _selection_sb: StyleBox = null

# render
var _blinking: bool = false
var _draw_source: Control = null
var _draw_mode: int = DM_BEAM
var _selection_rect: Rect2 = Rect2()



func set_draw_info(src: Control, mode: int) -> void:
	if src != _draw_source: 
		if _draw_source != null:
			_draw_source.resized.disconnect(queue_redraw)
		
		_draw_source = src
		
		if _draw_source != null:
			_draw_source.resized.connect(queue_redraw)
	
	_draw_mode = mode
	
	queue_reposition()
	queue_redraw()

func enforce_visible() -> void:
	_blink_timer.start()
	
	if not _blinking:
		_blinking = true
		queue_redraw()

func queue_reposition() -> void:
	if _pending_reposition:
		return
	
	_pending_reposition = true
	_reposition.call_deferred()

func queue_reposition_visible() -> void:
	queue_reposition()
	enforce_visible()

func queue_reposition_after(s: Signal) -> void:
	await s
	queue_redraw()

func queue_redraw_after(s: Signal) -> void:
	await s
	queue_redraw()



func get_active_state() -> TextCursorState:
	if _active_state != -1:
		return get_child(_active_state) as TextCursorState
	
	return null

func activate_state(new_state: int) -> void:
	if new_state == _active_state:
		return
	
	if _active_state != -1:
		get_active_state().notify_deactivation()
		
	_active_state = new_state
	
	if _active_state != -1:
		get_active_state().notify_activation(self, context)

func deactivate_state() -> void:
	if _active_state != -1:
		get_active_state().notify_deactivation()
	
	_active_state = -1

func get_active_state_path() -> String:
	if _active_state == -1:
		return ""
	
	var current := get_child(_active_state) as TextCursorState
	var res = current.get_state_name()
	
	while true:
		current = current.get_active_substate()
		
		if current == null:
			break
		
		res += "/" + current.get_state_name()
	
	return res



func queue_state_resync() -> void:
	if _pending_state_resync:
		return
	
	_pending_state_resync = true
	_state_resync.call_deferred()

func sync_state_with_context() -> void:
	if  context.current_is_expression():
		activate_state(S_EXPRESSION)
	else:
		deactivate_state()



func position_over(ctrl: Control) -> void:
	position = ctrl.global_position - get_parent().global_position

func position_over_input(input: TextInput, idx: int) -> void:
	position_over(input)
	
	if input.content.is_empty():
		position.x += input.size.x / 2.0
	
	else:
#		var font = input.get_font()
#		var font_size = input.get_font_size()
#		var left_margin = input.get_left_margin()
#
#		var substr = input.content.substr(0, idx)
#		var substr_size = font.get_string_size(substr, 0, -1, font_size)
#
#		position.x += left_margin + substr_size.x
		position.x += input.get_index_position(idx)



func update_selection() -> void:
	var min_x: float = INF
	var max_x: float = -INF
	var min_y: float = INF
	var max_y: float = -INF
	
	for selected in context.selection:
		var p = selected.global_position - get_parent().global_position
		
		min_x = min(min_x, p.x)
		max_x = max(max_x, p.x + selected.size.x)
		min_y = min(min_y, p.y)
		max_y = max(max_y, p.y + selected.size.y)
	
	var pos = Vector2(min_x, min_y)
	var siz = Vector2(max_x - min_x, max_y - min_y)
	_selection_rect = Rect2(pos, siz)



func repeat_input() -> void:
	_repeat_input = true



func _init() -> void:
	_add_states()
	
	_blink_timer = Timer.new()
	_blink_timer.one_shot = false
	_blink_timer.autostart = true
	_blink_timer.timeout.connect(_on_blink_timer_timeout)
	_blink_timer.wait_time = 1.0 / _blink_frequency
	add_child(_blink_timer)
	
	context = TextCursorContext.new()
	context.changed.connect(_on_context_changed)
	context.buffer_changed.connect(queue_redraw)
	add_child(context)
	
	mouse_filter = Control.MOUSE_FILTER_IGNORE

func _gui_input(ev: InputEvent) -> void:
	_repeat_input = true
	while _repeat_input:
		_repeat_input = false
		_dispatch_input(ev)

func _draw() -> void:
	if context.has_selection():
		_draw_selection()
	
	if _blinking and _draw_source != null:
		_draw_cursor()
	
	if not context.buffer.is_empty():
		_draw_buffer()

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_THEME_CHANGED:
			_focused_color = UtilsTheme.get_color(self, &"focused_color", &"Cursor")
			_unfocused_color = UtilsTheme.get_color(self, &"unfocused_color", &"Cursor")
			_blink_frequency = UtilsTheme.get_constant(self, &"blink_frequency", &"Cursor")
			_beam_width = UtilsTheme.get_constant(self, &"beam_width", &"Cursor")
			_border_width = UtilsTheme.get_constant(self, &"border_width", &"Cursor")
			_buffer_font = UtilsTheme.get_font(self, &"font", &"Tile")
			_buffer_font_size = UtilsTheme.get_font_size(self, &"font_size", &"Tile")
			_buffer_color = UtilsTheme.get_color(self, &"buffer_color", &"Cursor")
			_buffer_background = UtilsTheme.get_stylebox(self, &"buffer_background", &"Cursor")
			_buffer_left_margin = UtilsTheme.get_constant(self, &"buffer_left_margin", &"Cursor")
			_buffer_right_margin = UtilsTheme.get_constant(self, &"buffer_right_margin", &"Cursor")
			_buffer_top_margin = UtilsTheme.get_constant(self, &"buffer_top_margin", &"Cursor")
			_buffer_bottom_margin = UtilsTheme.get_constant(self, &"buffer_bottom_margin", &"Cursor")
			_selection_sb = UtilsTheme.get_stylebox(self, &"selection", &"Cursor")
			
			if _blink_timer != null:
				_blink_timer.wait_time = 1.0 / float(_blink_frequency)



func _draw_cursor() -> void:
	var src_size = _draw_source.size
	
	match _draw_mode:
		DM_BEAM:
			var from = Vector2.ZERO
			var to = Vector2(0, src_size.y)
			
#			if from == to:
#				push_error("height == 0")
			
			if has_focus():
				draw_line(from, to, _focused_color, _beam_width)
			else:
				draw_line(from, to, _unfocused_color, _beam_width)
			
		DM_RECT:
			var rect = Rect2(Vector2.ZERO, src_size)
			
			if has_focus():
				draw_rect(rect, _focused_color, false, _border_width)
			else:
				draw_rect(rect, _unfocused_color, false, _border_width)

func _draw_buffer() -> void:
	var buf_size = _buffer_font.get_string_size(context.buffer, 0, -1, _buffer_font_size)
	
	var height = _draw_source.size.y if _draw_source != null else 0.0
	var rect = Rect2(
		0, height, 
		buf_size.x + _buffer_left_margin + _buffer_right_margin,
		buf_size.y + _buffer_top_margin + _buffer_bottom_margin
	)
	
	var y = rect.position.y + _buffer_top_margin + _buffer_font.get_ascent(_buffer_font_size)
	
	_buffer_background.draw(get_canvas_item(), rect)
	_buffer_font.draw_string(
		get_canvas_item(),
		Vector2(_buffer_left_margin, y),
		context.buffer, 0, -1, _buffer_font_size, _buffer_color
	)

func _draw_selection() -> void:
	var pos = _selection_rect.position
	var siz = _selection_rect.size
	
	pos -= position
	
	_selection_sb.draw(get_canvas_item(), Rect2(pos, siz))



func _dispatch_input(ev: InputEvent) -> void:
	if ev is InputEventKey:
		if _active_state != -1 and get_active_state().handle_key_event(ev):
			accept_event()
			return 
			
		var txt = char(ev.unicode)
		if not txt.is_empty():
			if _active_state != -1 and get_active_state().handle_text_input(txt):
				accept_event()
				return 
	
		if _fallback_key_handler(ev):
			accept_event()
			return 
		
		if not txt.is_empty() and _fallback_text_input_handler(txt):
			accept_event()
			return 



func _reposition() -> void:
	if _active_state == -1 or not get_active_state().handle_cursor_reposition():
		_fallback_reposition_handler()
	
	_pending_reposition = false

func _state_resync() -> void:
	deactivate_state()
	sync_state_with_context()
	
	_pending_state_resync = false



func _add_states() -> void:
	add_child(StateExpression.new())



func _fallback_key_handler(ev: InputEventKey) -> bool:
	if ev.pressed:
		match ev.keycode:
			KEY_LEFT:
				if ev.shift_pressed:
					context.goto_and_select_left()
				else:
					context.goto_left_tile()
					context.unselect_all()
					
				update_selection()
				enforce_visible()
				queue_redraw()
				return true
			
			KEY_RIGHT:
				if ev.shift_pressed:
					context.goto_and_select_right()
				else:
					context.goto_right_tile()
					context.unselect_all()
				
				update_selection()
				enforce_visible()
				queue_redraw()
				return true
	
	return false

func _fallback_text_input_handler(_txt: String) -> bool:
	return false

func _fallback_reposition_handler() -> void:
	if context.current != null:
		position_over(context.current)



func _on_blink_timer_timeout() -> void:
	_blinking = not _blinking
	queue_redraw()

func _on_context_changed() -> void:
	var prev_state = _active_state
	
	sync_state_with_context()
	
	if _active_state == prev_state and _active_state != -1:
		get_active_state().notify_context_change()
