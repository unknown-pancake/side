class_name TextCursorContext
extends Node

signal changed()
signal buffer_changed()



enum {
	NONE = 0,
	EXPR_LEFT_PAR,
	EXPR_RIGHT_PAR,
	EXPR_BIN_OP,
	EXPR_INT,
	EXPR_HOLE
}

enum {
	# context changed without motions
	M_NONE,
	# context changed with a direct motion
	M_DIRECT,
	# context changed by moving the cursor to the left
	M_LEFT,
	# context changed by moving the cursor to the right
	M_RIGHT
}

enum {
	P_UNKNOWN = 0,
	P_BEGIN,
	P_MIDDLE,
	P_END,
	P_BEGIN_END,
}



var buffer: String:
	get: return _buffer
	set(v):
		_buffer = v
		
		if not v.is_empty():
			_buffer_reset_timer.start()
		else:
			_buffer_reset_timer.stop()
		
		buffer_changed.emit()



# general context
var current: TextTile
var next: TextTile
var last_motion: int = M_DIRECT
var position: int = P_UNKNOWN
var selection: Array[TextTile] = []

# input context
var input_index: int

# short term buffer
var _buffer: String
var _buffer_reset_timer: Timer



func current_is(v: int) -> bool:
	return current.context == v

func next_is(v: int) -> bool:
	if next == null:
		return false
	
	return next.context == v

func position_is(v: int) -> bool:
	if position == P_BEGIN_END:
		return v == P_BEGIN or v == P_END or v == P_BEGIN_END
	else:
		return position == v



func current_is_expression() -> bool:
	match current.context:
		EXPR_LEFT_PAR: return true
		EXPR_RIGHT_PAR: return true
		EXPR_BIN_OP: return true
		EXPR_HOLE: return true
		EXPR_INT: return true
		_: return false

func current_is_parenthesis() -> bool:
	match current.context:
		EXPR_LEFT_PAR: return true
		EXPR_RIGHT_PAR: return true
		_: return false

func current_is_operator() -> bool:
	match current.context:
		EXPR_BIN_OP: return true
		_: return false

func current_is_input() -> bool:
	match current.context:
		EXPR_HOLE: return true
		EXPR_INT: return true
		_: return false



func is_at_input_beginning() -> bool:
	if current_is_input():
		return input_index == 0
	
	return false

func is_at_input_end() -> bool:
	if current_is_input():
		return input_index == current.content.length()
	
	return false



func update_context_direct(new_current: TextTile, update := true) -> void:
	current = new_current
	next = TextTreeUtils.find_tile(new_current, false)
	last_motion = M_DIRECT
	
	if update:
		update_context()

func update_context_relative(new_current: TextTile, left: bool, update := true) -> void:
	if new_current != null:
		# moving left
		if left:
			# no need to look it up in that case
			next = current
		
		current = new_current
		last_motion = M_LEFT if left else M_RIGHT
		
		if not left:
			next = TextTreeUtils.find_tile(new_current, false)
	
	if update:
		update_context()

func update_context_surrounding(update := true) -> void:
	next = TextTreeUtils.find_tile(current, false)
	#last_motion = M_NONE
	
	if update:
		update_context()

func update_context_current(new_current: TextTile, update := true) -> void:
	current = new_current
	
	if update:
		update_context()

func update_context() -> void:
	update_position()
	changed.emit()

func update_position() -> void:
	if current_is_input():
		var begin = is_at_input_beginning()
		var end = is_at_input_end()
		
		if begin and end:
			position = P_BEGIN_END
		elif begin:
			position = P_BEGIN
		elif end:
			position = P_END
		else:
			position = P_MIDDLE
	
	elif current_is(EXPR_LEFT_PAR):
		position = P_BEGIN
	
	elif current_is(EXPR_RIGHT_PAR):
		position = P_END
	
	else:
		position = P_UNKNOWN



func goto_first_tile(root: Control, update := true) -> void:
	var tile = TextTreeUtils.find_tile(root, false)
	if tile != null:
		update_context_direct(tile, update)

func goto_right_tile(update := true) -> void:
	update_context_relative(next, false, update)

func goto_left_tile(update := true) -> void:
	var prev = TextTreeUtils.find_tile(current, true)
	update_context_relative(prev, true, update)

func goto_tile_after_deletion_of(deleted: Control, to_left: bool, update := true) -> bool:
	var pred = func(x: Control):
		return x is TextTile and not x.marked_for_deletion
	
	var res = TextExpressionUtils.find_node_down_and_up(deleted, to_left, pred)
	
	# if we didn't find a suitable node in the desired direction
	if res == null:
		# search for a suitable node in the opposite direction
		res = TextExpressionUtils.find_node_down_and_up(deleted, not to_left, pred)
		to_left = not to_left
	
	# if in either directions no suitable node was found
	if res == null:
		return false
	
	# never update the context here
	update_context_direct(res, false)
	# set the last motion
	last_motion = M_LEFT if to_left else M_RIGHT
	
	# and update if necessary
	if update:
		update_context()
	
	return true



func move_right_within_input(update := true) -> bool:
	if not current_is_input():
		return false
	
	if input_index >= current.content.length():
		return false
	
	input_index += 1
	
	if update: 
		update_context()
	
	return true

func move_left_within_input(update := true) -> bool:
	if not current_is_input():
		return false
	
	if input_index == 0:
		return false
	
	input_index -= 1
	
	if update: 
		update_context()
	
	return true

func set_input_index_to_begin(update := true) -> void:
	input_index = 0
	
	if update:
		update_context()

func set_input_index_to_end(update := true) -> void:
	if current_is_input():
		input_index = current.content.length()
	else:
		input_index = 0
	
	if update:
		update_context()

func set_input_index(idx: int, update := true) -> void:
	input_index = idx
	sanitize_input_index()
	
	if update:
		update_context()

func sanitize_input_index() -> void:
	if not current_is_input():
		return
	
	if input_index < 0:
		input_index = 0
	elif input_index > current.content.length():
		input_index = current.content.length()



func push_operator_char(txt: String) -> int:
	var buf = _buffer + txt
	# -2 : invalid
	# -1 : valid, but incomplete
	var res = -2
	
	match buf:
		"+":   res = AstOperators.Binary.add
		"-":   res = AstOperators.Binary.sub
		"*":   res = AstOperators.Binary.mul
		"/":   res = AstOperators.Binary.div
		"%":   res = AstOperators.Binary.mod
		"&":   res = AstOperators.Binary.bw_and
		"|":   res = AstOperators.Binary.bw_or
		"^":   res = AstOperators.Binary.bw_xor
		"<<":  res = AstOperators.Binary.shl
		">>":  res = AstOperators.Binary.shr
		"and": res = AstOperators.Binary.l_and
		"or":  res = AstOperators.Binary.l_or
		"=":   res = AstOperators.Binary.eq
		"!=":  res = AstOperators.Binary.ne
		"<":   res = AstOperators.Binary.lt
		"<=":  res = AstOperators.Binary.le
		">":   res = AstOperators.Binary.gt
		">=":  res = AstOperators.Binary.ge
		
		"!": res = -1
		"a", "an": res = -1
		"o": res = -1
	
	if res == -2:
		buffer = ""
	else:
		buffer = buf
	
	return res



func has_selection() -> bool:
	return not selection.is_empty()

func select_current() -> void:
	selection = [current]

func unselect_all() -> void:
	selection = []

func goto_and_select_left() -> void:
	var left = TextTreeUtils.find_tile(current, true)
	
	if not has_selection():
		selection.push_back(current)
	
	if left != null:
		# selecting to the right, but moving left
		if has_selection() and selection.back() == current and selection.size() > 1:
			selection.pop_back()
		else:
			selection.push_front(left)
		
		update_context_relative(left, true)

func goto_and_select_right() -> void:
	if not has_selection():
		selection.push_back(current)
	
	if next != null:
		# selecting to the left, but moving right
		if has_selection() and selection.front() == current and selection.size() > 1:
			selection.pop_front()
		else:
			selection.push_back(next)
		
		update_context_relative(next, false)



func _init() -> void:
	_buffer_reset_timer = Timer.new()
	_buffer_reset_timer.wait_time = 0.5
	_buffer_reset_timer.one_shot = true
	_buffer_reset_timer.timeout.connect(_on_buffer_reset_timer_timeout)
	add_child(_buffer_reset_timer)



func _on_buffer_reset_timer_timeout() -> void:
	_buffer = ""
	buffer_changed.emit()
