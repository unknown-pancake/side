class_name TextCursorState
extends Node



var _cursor: TextCursor
var _context: TextCursorContext
var _active_substate: int = -1



func get_active_substate() -> TextCursorState:
	if _active_substate != -1:
		return get_child(_active_substate) as TextCursorState
	
	return null

func get_state_name() -> String:
	return ""



func notify_activation(cur: TextCursor, ctx: TextCursorContext) -> void:
	_cursor = cur
	_context = ctx
	
	notify_context_change(true)

func notify_deactivation() -> void:
	_cursor = null
	_context = null
	
	_deactivate_substate()

func notify_context_change(from_activation := false) -> void:
	var prev_substate = _active_substate
	_sync_to_context()
	
	if _active_substate == -1:
		_handle_context_change(from_activation)
	elif _active_substate == prev_substate:
		get_active_substate().notify_context_change(false)



func handle_key_event(ev: InputEventKey) -> bool:
	if _pre_handle_key_event(ev):
		return true
	
	if _active_substate != -1 and get_active_substate().handle_key_event(ev):
		return true
	
	return _unhandled_key_event(ev)

func handle_text_input(txt: String) -> bool:
	if _pre_handle_text_input(txt):
		return true
	
	if _active_substate != -1 and get_active_substate().handle_text_input(txt):
		return true
	
	return _unhandled_text_input(txt)

func handle_cursor_reposition() -> bool:
	if _pre_handle_cursor_reposition():
		return true
	
	if _active_substate != -1 and get_active_substate().handle_cursor_reposition():
		return true
	
	return _unhandled_cursor_reposition()



func _pre_handle_key_event(_ev: InputEventKey) -> bool:
	return false

func _unhandled_key_event(_ev: InputEventKey) -> bool:
	return false

func _pre_handle_text_input(_txt: String) -> bool:
	return false

func _unhandled_text_input(_txt: String) -> bool:
	return false

func _pre_handle_cursor_reposition() -> bool:
	return false

func _unhandled_cursor_reposition() -> bool:
	return false



func _sync_to_context() -> void:
	pass

func _handle_context_change(_from_activation: bool) -> void:
	_cursor.set_draw_info(_context.current, TextCursor.DM_RECT)



func _activate_substate(idx: int) -> void:
	if idx == _active_substate:
		return
	
	if _active_substate != -1:
		get_active_substate().notify_deactivation()
	
	_active_substate = idx
	
	if _active_substate != -1:
		get_active_substate().notify_activation(_cursor, _context)

func _deactivate_substate() -> void:
	if _active_substate != -1:
		get_active_substate().notify_deactivation()
	
	_active_substate = -1
