
extends Control


signal error_generated(msg: String)





var context: TextCursorContext
var current: Control

# drawing stuff
var _blinking: bool = false
var _show_rect: bool = false
var _show_rect_control: Control = null

# theme stuff
var _focused_color: Color = Color.BLACK
var _unfocused_color: Color = Color.BLACK
var _blink_frequency: int = 0
var _width: int = 0

# children nodes
var _blink_timer: Timer
var _buffer_reset_timer: Timer

# other
var _input_index: int
var _pending_reposition: bool = false

var _buffer: String
var _next: TextTile = null



func move_to_first_tile(root: Control) -> void:
	_set_current(TextTreeUtils.find_tile(root, false))
	_queue_reposition()
	queue_redraw()



func _init() -> void:
	_blink_timer = Timer.new()
	_blink_timer.one_shot = false
	_blink_timer.autostart = true
	_blink_timer.timeout.connect(_on_blink_timer_timeout)
	_blink_timer.wait_time = 1.0 / _blink_frequency
	add_child(_blink_timer)
	
	_buffer_reset_timer = Timer.new()
	_buffer_reset_timer.wait_time = 0.5
	_buffer_reset_timer.one_shot = true
	_buffer_reset_timer.timeout.connect(_on_buffer_reset_timer_timeout)
	add_child(_buffer_reset_timer)
	
	context = TextCursorContext.new()
	add_child(context)

func _gui_input(ev: InputEvent) -> void:
	if ev is InputEventKey:
		if ev.pressed:
			if _handle_key_press(ev):
				accept_event()
				return
			
			var ucode = char(ev.unicode)
			
			if not ucode.is_empty():
				_handle_text_input(char(ev.unicode))
		else:
			if ev.keycode == KEY_CTRL:
				_handle_ctrl_key(false)

#func _draw() -> void:
#	if current == null or not _blinking:
#		return
#
#	var half_width = _width / 2.0
#
#	if not _show_rect and current.is_input():
#		var height = current.size.y
#		var rect = Rect2(-half_width, 0, _width, height)
#
#		if rect.size.y == 0:
#			printerr("height = 0")
#
#		if has_focus():
#			draw_rect(rect, _focused_color)
#		else:
#			draw_rect(rect, _unfocused_color)
#	else:
#		if _show_rect_control != null:
#			var p = _show_rect_control
#			var rect = Rect2(-half_width, -half_width, p.size.x + _width, p.size.y + _width)
#
#			draw_rect(rect, _focused_color, false, _width)
#
#		else:
#			var rect = Rect2(-half_width, -half_width, current.size.x + _width, current.size.y + _width)
#
#			if has_focus():
#				draw_rect(rect, _focused_color, false, _width)
#			else:
#				draw_rect(rect, _unfocused_color, false, _width)

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_THEME_CHANGED:
			_focused_color = UtilsTheme.get_color(self, &"focused_color", &"Cursor")
			_unfocused_color = UtilsTheme.get_color(self, &"unfocused_color", &"Cursor")
			_blink_frequency = UtilsTheme.get_constant(self, &"blink_frequency", &"Cursor")
			_width = UtilsTheme.get_constant(self, &"width", &"Cursor")
			
			if _blink_timer != null:
				_blink_timer.wait_time = 1.0 / float(_blink_frequency)



func _move_left(repos := true) -> void:
	if current.is_input() and _move_left_within_input():
		return
	
	var prev = _find_tile_before(current)
	
	if prev != null:
		_set_current(prev)
		_focus_from_right()
		
		if repos:
			_queue_reposition()
			queue_redraw()

func _move_right(repos := true) -> void:
	if current.is_input() and _move_right_within_input():
		return
	
	var next = _find_tile_after(current)
	
	if next != null:
		_set_current(next)
		_focus_from_left()
		
		if repos:
			_queue_reposition()
			queue_redraw()

func _move_left_within_input() -> bool:
	_input_index -= 1
	
	if _input_index >= 0:
		_queue_reposition()
		return true
	
	_input_index = 0
	return false

func _move_right_within_input() -> bool:
	_input_index += 1
	
	if _input_index <= current.content.length():
		_queue_reposition()
		return true
	
	_input_index = current.content.length()
	return false



func _focus_from_left() -> void:
	if current.is_input():
		_input_index = 0

func _focus_from_right() -> void:
	if current.is_input():
		_input_index = current.content.length()



func _reposition() -> void:
	var pos = current.global_position - get_parent().global_position
	
	if _show_rect:
		if _show_rect_control != null:
			pos = _show_rect_control.global_position - get_parent().global_position
		
	elif current.is_input():
		pos = _reposition_within_input(pos)
	
	position = pos
	_reset_blinking()
	
	_pending_reposition = false

func _reposition_within_input(pos: Vector2) -> Vector2:
	if not current.content.is_empty():
		var font = current.get_font()
		var font_size = current.get_font_size()
		var left_margin = current.get_left_margin()
		
		var substr = current.content.substr(0, _input_index)
		var substr_size = font.get_string_size(substr, 0, -1, font_size)
		
		pos.x += left_margin + substr_size.x
	else:
		pos.x += current.size.x  / 2
	
	return pos

func _queue_reposition() -> void:
	if _pending_reposition:
		return
	
	_reposition.call_deferred()
	_pending_reposition = true



func _reposition_after_resort_of(c: Control) -> void:
	await c.sort_children
	_queue_reposition()

func _reposition_after_resize_of(c: Control) -> void:
	await c.resized
	_queue_reposition()
	queue_redraw()



func _find_tile_after(tile: Control) -> Control:
	if tile == current:
		return _next
	
	return TextTreeUtils.find_tile(tile, false, false)

func _find_tile_before(tile: Control) -> Control:
	return TextTreeUtils.find_tile(tile, true, false)

func _find_tile_after_deletion(
	deleted: Control,
	to_left: bool
) -> Control:
	var parent = deleted.get_parent()
	
	var pred = func(x: Control):
		if deleted.is_ancestor_of(x):
			return false
		
		if x.get_parent() == parent:
			if TextExpressionUtils.is_binary_operator(x):
				return false
			elif TextExpressionUtils.is_group_parenthesis(x):
				return false
		
		return true
	
	var res = TextExpressionUtils.find_node_down_and_up(current, to_left, pred)
	
	if res != null:
		return res
	else:
		return TextExpressionUtils.find_node_down_and_up(current, not to_left, pred)



func _handle_key_press(ev: InputEventKey) -> bool:
	match ev.keycode:
		KEY_LEFT:
			if ev.ctrl_pressed:
				_move_node(true)
			else:
				_move_left()
			return true
		KEY_RIGHT:
			if ev.ctrl_pressed:
				_move_node(false)
			else:
				_move_right()
			return true
		
		KEY_BACKSPACE:
			return _handle_backspace_key()
			
		KEY_DELETE:
			return _handle_delete_key()
		
		KEY_CTRL:
			if not ev.echo:
				_handle_ctrl_key(true)

	return false

func _handle_ctrl_key(pressed: bool) -> void:
	if pressed:
		_enable_rect_display(current)
	else:
		_disable_rect_display()

func _handle_backspace_key() -> bool:
	if current.is_input():
		if current.content.is_empty():
			_delete_node(true)
		else:
			_remove_char_from_input(true)
			
		return true
	
	else:
		if TextExpressionUtils.is_group_parenthesis(current):
			_ungroup_expression(true)
			return true
	
	return false

func _handle_delete_key() -> bool:
	if current.is_input():
		if current.content.is_empty():
			_delete_node(false)
		else:
			_remove_char_from_input(false)
			
		return true
		
	else:
		if TextExpressionUtils.is_group_parenthesis(current):
			_ungroup_expression(false)
			return true
	
	return false

func _handle_text_input(txt: String) -> void:
	if current.is_input():
		_insert_char_in_input(txt)
	elif current.is_in_group(&"binary_operator"):
		_mutate_binary_expression_operator(txt)
	elif _exception_tile_doesnt_support_text_input(txt):
		error_generated.emit("The current tile doesn't support text input.")



func _insert_char_in_input(txt: String) -> void:
	if not current.is_char_accepted(_input_index, txt):
		if _exception_invalid_char_in_input(txt):
			error_generated.emit("The character '" + txt.c_escape() + "' isn't accepted here.")
		
		return
	
	TextExpressionUtils.insert_text_in_input(current, txt, _input_index)
	
	_input_index += txt.length()
	_queue_reposition()
	accept_event()

func _remove_char_from_input(to_left: bool) -> void:
	if to_left:
		TextExpressionUtils.remove_text_from_input(current, _input_index - 1, 1)
		
		_input_index = max(0, _input_index - 1)
		_queue_reposition()
	
	else:
		TextExpressionUtils.remove_text_from_input(current, _input_index, 1)



func _handle_edge_insertion(before: bool, txt: String) -> bool:
	if current is TextExpressionHole and _handle_expression_hole_mutation(txt):
		return true
	
	match txt:
		"(": 
			_group_expression()
			return true
		")": 
			var next = _find_tile_after(current)
			if TextExpressionUtils.is_group_parenthesis(next) and next.content == ")":
				_move_right(false)
				_move_right()
			return true
		"+":
			_insert_binary_expression(txt, AstOperators.Binary.add, before)
			return true
		"-":
			_insert_binary_expression(txt, AstOperators.Binary.sub, before)
			return true
		"*":
			_insert_binary_expression(txt, AstOperators.Binary.mul, before)
			return true
		"/":
			_insert_binary_expression(txt, AstOperators.Binary.div, before)
			return true
		"%":
			_insert_binary_expression(txt, AstOperators.Binary.mod, before)
			return true
		"&":
			_insert_binary_expression(txt, AstOperators.Binary.bw_and, before)
			return true
		"|":
			_insert_binary_expression(txt, AstOperators.Binary.bw_or, before)
			return true
		"^":
			_insert_binary_expression(txt, AstOperators.Binary.bw_xor, before)
			return true
		"<":
			_insert_binary_expression(txt, AstOperators.Binary.lt, before)
			return true
		">":
			_insert_binary_expression(txt, AstOperators.Binary.gt, before)
			return true
		"!":
			_insert_binary_expression(txt, AstOperators.Binary.ne, before)
			return true
		"=":
			_insert_binary_expression(txt, AstOperators.Binary.eq, before)
			return true
	
	return false

func _handle_expression_hole_mutation(txt: String) -> bool:
	# an int was typed
	if txt.is_valid_int(): 
		_mutate_expression_hole_to_int(txt)
		return true
	
	return false



func _mutate_binary_expression_operator(op: String) -> void:
	var valids = {
		"+": AstOperators.Binary.add,
		"-": AstOperators.Binary.sub,
		"*": AstOperators.Binary.mul,
		"/": AstOperators.Binary.div,
		"%": AstOperators.Binary.mod,
		"&": AstOperators.Binary.bw_and,
		"|": AstOperators.Binary.bw_or,
		"^": AstOperators.Binary.bw_xor,
		"<<": AstOperators.Binary.shl,
		">>": AstOperators.Binary.shr,
		"and": AstOperators.Binary.l_and,
		"or": AstOperators.Binary.l_or,
		"=": AstOperators.Binary.eq,
		"!=": AstOperators.Binary.ne,
		"<": AstOperators.Binary.lt,
		"<=": AstOperators.Binary.le,
		">": AstOperators.Binary.gt,
		">=": AstOperators.Binary.ge,
		"!": -1
	}
	
	var new_buf = _buffer + op
	
	if valids.has(new_buf):
		var new_op = valids[new_buf]
		if new_op != -1:
			current.get_parent().set_underlined(false)
			TextExpressionUtils.mutate_binary_operator(current.get_parent(), new_op)
			current.get_parent().set_underlined(true)
		
		_set_buffer(new_buf)
	else:
		if _exception_invalid_bin_op_mutation(op):
			error_generated.emit("'" + new_buf.c_escape() + "' is not a valid operator.")
		
		_reset_buffer()

func _mutate_expression_hole_to_int(txt: String) -> void:
	current.get_parent().set_underlined(false)
	var lit = TextExpressionUtils.mutate_hole_to_integer_literal(current, txt.to_int())
	_set_current(lit)
	current.get_parent().set_underlined(true)
	
	_focus_from_right()
	_queue_reposition()
	queue_redraw()

func _insert_binary_expression(txt: String, op: AstOperators.Binary, before: bool) -> void:
	
	current.get_parent().set_underlined(false)
	
	# if we are on a `)`
	if TextExpressionUtils.is_group_parenthesis(current):
		if current.content == ")":
			TextExpressionUtils.insert_binary_expression(current.get_parent(), op, not before)
		else:
			current.get_parent().set_underlined(true)
			return
	else:
		TextExpressionUtils.insert_binary_expression(current, op, not before)
	
	_cache_next_tile()
	
	if before:
		_move_left(false)
	else:
		_move_right(false)
	
	current.get_parent().set_underlined(true)
	
	_queue_reposition()
	queue_redraw()
	
	# the function will focus the binary operator
	# save the input in the buffer is case the user wants to input a multichar
	# operator (such as <=, !=, or >>)
	_set_buffer(txt)



func _group_expression() -> void:
	current.get_parent().set_underlined(false)
	TextExpressionUtils.group_expression(current)
	current.get_parent().set_underlined(true)
	
	_cache_next_tile()
	_queue_reposition()
	queue_redraw()

func _ungroup_expression(move_left: bool) -> void:
	# here current is a parenthesis in `group`
	# since the cursor is on a parenthesis that will diseappear, it must
	# be moved to a neighbour tile
	var group = current.get_parent()
	
	if current.get_index() == 0:
		if move_left:
			var tile = _find_tile_before(current)
			if tile == null:
				_move_right()
			else:
				_move_left()
		else:
			_move_right()
	else:
		if move_left:
			_move_left()
		else:
			var tile = _find_tile_after(current)
			if tile == null:
				_move_left()
			else:
				_move_right()
	
	TextExpressionUtils.ungroup_expression(group)
	
	if move_left:
		_queue_reposition()
	else:
		_reposition_after_resort_of(group.get_parent())
	
	queue_redraw()

func _move_node(to_left: bool) -> void:
	current.get_parent().set_underlined(false)
	TextExpressionUtils.move_node(current, to_left)
	current.get_parent().set_underlined(true)
	_queue_reposition()

func _delete_node(to_left: bool) -> void:
	var to_delete = current
	var prev = _find_tile_after_deletion(to_delete, to_left)
	
	if prev == null:
		error_generated.emit("Unable to find a suitable tile to move the cursor after the deletion.")
		return
	
	current = null
	
	TextExpressionUtils.delete_node(to_delete)
	
	_set_current(prev)
	_focus_from_right()
	_queue_reposition()
	queue_redraw()



func _exception_invalid_bin_op_mutation(txt: String) -> bool:
	var next = _find_tile_after(current)
	
	if next is TextExpressionHole:
		_move_right(false)
		# TODO: feels weird
		_handle_edge_insertion(true, txt)
		_reposition_after_resize_of(current)
		return false
	
	return true

func _exception_invalid_char_in_input(txt: String) -> bool:
	if current.is_input():
		if _input_index == 0:
			if _handle_edge_insertion(true, txt):
				return false
		elif _input_index == current.content.length():
			if _handle_edge_insertion(false, txt):
				return false

	return true

func _exception_tile_doesnt_support_text_input(txt: String) -> bool:
	if TextExpressionUtils.is_group_parenthesis(current) and current.content == ")":
		if _handle_edge_insertion(false, txt):
			return false
		
	return true



func _set_buffer(v: String) -> void:
	_buffer = v
	_buffer_reset_timer.start()

func _reset_buffer() -> void:
	_buffer = ""
	_buffer_reset_timer.stop()



func _set_current(v: Control) -> void:
	if current != null:
		if not current.is_input():
			current.resized.disconnect(_on_current_resized)
		
		current.get_parent().set_underlined(false)
	
	current = v
	
	if current != null:
		if not current.is_input():
			current.resized.connect(_on_current_resized)
		
		current.get_parent().set_underlined(true)
		_cache_next_tile()

func _cache_next_tile() -> void:
	_next = TextTreeUtils.find_tile(current, false, false)



func _reset_blinking() -> void:
	_blink_timer.start()
	
	if not _blinking:
		_blinking = true
		queue_redraw()



func _enable_rect_display(control: Control) -> void:
	_show_rect = true
	_show_rect_control = control
	
	if TextExpressionUtils.is_binary_operator(control):
		_show_rect_control = control.get_parent()
	
	if not TextExpressionUtils.is_group_parenthesis(control):
		while TextExpressionUtils.is_within_group(_show_rect_control):
			_show_rect_control = _show_rect_control.get_parent()
	
	queue_redraw()
	_queue_reposition()

func _disable_rect_display() -> void:
	_show_rect = false
	_show_rect_control = null
	queue_redraw()
	_queue_reposition()
	accept_event()



func _on_current_resized() -> void:
	queue_redraw()

func _on_blink_timer_timeout() -> void:
	_blinking = not _blinking
	queue_redraw()

func _on_buffer_reset_timer_timeout() -> void:
	_reset_buffer()
