class_name TextExpressionGroup
extends TextGroup


var node: AstExpressionGroup

var _left_par: TextToken
var _inner: Control
var _right_par: TextToken



func get_left_parenthesis() -> TextToken:
	return _left_par

func get_right_parenthesis() -> TextToken:
	return _right_par

func get_inner_control() -> Control:
	return _inner

func set_inner_control(c: Control) -> void:
	_inner = c
	
	if c != null and c.get_parent() == null:
		add_child(c)
		move_child(c, 1)



func _init() -> void:
	_left_par = TextToken.new()
	_left_par.content = "("
	_left_par.theme_type_variation = &"Symbol"
	_left_par.context = TextCursorContext.EXPR_LEFT_PAR
	_left_par.add_to_group(&"group_parenthesis")
	add_child(_left_par)
	
	_right_par = TextToken.new()
	_right_par.content = ")"
	_right_par.theme_type_variation = &"Symbol"
	_right_par.context = TextCursorContext.EXPR_RIGHT_PAR
	_right_par.add_to_group(&"group_parenthesis")
	add_child(_right_par)
