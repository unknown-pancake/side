class_name TextExpressionBinary
extends TextGroup



var node: AstExpressionBinary


var _left: Control
var _operator: TextToken
var _right: Control



func get_left_control() -> Control:
	return _left

func set_left_control(c: Control) -> void:
	_left = c
	
	if c != null and c.get_parent() == null:
		add_child(c)
		_order_children()

func get_operator_control() -> TextToken:
	return _operator

func get_right_control() -> Control:
	return _right

func set_right_control(c: Control) -> void:
	_right = c
	
	if c != null and c.get_parent() == null:
		add_child(c)
		_order_children()



func set_control(c: Control, is_left: bool) -> void:
	if is_left:
		set_left_control(c)
	else:
		set_right_control(c)

func get_control(left: bool) -> Control:
	if left:
		return get_left_control()
	else:
		return get_right_control()

func set_branch(v: AstExpression, is_left: bool) -> void:
	if is_left:
		node.left = v
	else:
		node.right = v



func setup_from_node() -> void:
	_operator.content = AstOperators.get_binary_representation(node.operator)



func _init() -> void:
	_operator = TextToken.new()
	_operator.content = "+"
	_operator.theme_type_variation = &"Symbol"
	_operator.context = TextCursorContext.EXPR_BIN_OP
	_operator.add_to_group(&"binary_operator")
	add_child(_operator)



func _order_children() -> void:
	move_child_at(_left, 0)
	move_child_at(_operator, 1)
	move_child_at(_right, 2)
